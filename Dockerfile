FROM quay.io/ansible/python-base
WORKDIR app
COPY . .
RUN pip install -r requirements.txt
EXPOSE 5000
CMD python src/index.py